package fr.isima.td_intro;

import android.content.Intent;

public class ActivityTwo extends TDActivity {
    private static final String TAG = ActivityTwo.class.getName();

    private Intent activityOneIntent;


    public ActivityTwo() {
        super(TAG, R.layout.activity_two);
    }

    @Override
    protected Intent getIntent(boolean forceNew) {
        if (forceNew || activityOneIntent == null) {
            activityOneIntent = new Intent(this, ActivityOne.class);
        }

        return activityOneIntent;
    }
}
